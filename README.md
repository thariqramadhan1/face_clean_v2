# Face_Clean_v2
This project clean face dataset using recursive similarity distance. Embedding use from this program is ArcFace ResNet 100.

## Installation
sudo apt-get install protobuf-compiler libprotoc-dev     
pip3 install onnx  
pip3 install mxnet  
pip3 install numpy  
pip3 install matplotlib 
pip3 install opencv-python 
pip3 install scikit-learn
pip3 install easydict   
pip3 install scikit-image               
pip3 install dlib

Don't forget to add mtcnn-model, helper.py, mtcnn_detector.py from https://github.com/onnx/models/tree/master/vision/body_analysis/arcface and download resnet100.onnx from https://s3.amazonaws.com/onnx-model-zoo/arcface/resnet100.onnx

## Use Code

Example : python main.py --input [desire_input_path] --output [desire_output_path] --steps[desire_steps]
- input : input path to folderID containing "train_n.jpg" image(s)is 

Dataset structure

datasetname
/label1
/label2
...

- output : desired output path, containing processed images
- steps : desired output path, containing processed images

## Explanation
This program do recursive cleaning per id.
There is several step from this program:
- Prepare file for raw and reference director

    If there is word "train" in filename from dataset folder copy file to reference, otherwise to raw
- Make csv file

    Generate csv file for train and raw image
- Embedding image

    Calculate embedding value from image of reference and raw
- Classifying image

    Copy reference image to below05 folder. Calculate distance from one by one reference to raw. Classify raw image from average distace.
- End recursive

    Stop recursive if satifying
