# Don't forget to add mtcnn-model, helper.py, mtcnn_detector.py from https://github.com/onnx/models/tree/master/vision/body_analysis/arcface 
# and download resnet100.onnx from https://s3.amazonaws.com/onnx-model-zoo/arcface/resnet100.onnx


# File core.py is function file extracted from arcface_inference.ipynb

from argparse import ArgumentParser
import os
import shutil
import pandas as pd
import numpy as np
import cv2
import csv
from pathlib import Path
import core

# Get parameter from user
# Example : python main.py --input [desire_input_path] --output [desire_output_path] --steps[desire_steps]
parser = ArgumentParser()
parser.add_argument('--input', type=str, required=True,
                    help='input path to folderID containing "train_n.jpg" image(s)')
parser.add_argument('--output', type=str, required=True,
                    help='desired output path, containing processed images')
parser.add_argument("--steps", type=int, default=1,
                    help="desired output path, containing processed images")
args = parser.parse_args()

# Main program


def main():
    print('======================================================')
    print('RECURSIVE FACE CLEANER')
    input_path = args.input
    output_path = args.output
    steps = args.steps
    ids = os.listdir(input_path)

    print("Input path : ", input_path)
    print("Output path : ", output_path)
    print("Recursive steps : ", steps)
    print("Total id : ", len(ids))
    print('======================================================')

    # Do recursive cleaning per ID
    for id in ids:
        id_path = os.path.join(input_path, id)

        # Prepare file for raw and reference directory
        prepare_file(id_path, output_path)

        # Do recursive cleaning
        for i in range(steps):
            print("Recursive for %s, steps %s" % (id, i))
            # Make csv file
            generate_csv(output_path, id)

            # Make classified directory per steps
            generate_filtered_dir(output_path, str(i), id)

            # Embedding reference image
            image_embedding(output_path, id, 'reference')

            # Embedding raw image
            image_embedding(output_path, id, 'raw')

            # Classifying image
            image_classifying(output_path, str(i), id)

            # End recursive if satisfying
            is_break = end_recursive(output_path, id, str(i), str(steps))
            if is_break:
                print("End recursive for %s, total recursive %s" % (id, i))
                break

# Prepare file for raw and reference directory


def prepare_file(input_path, output_path):
    files = os.listdir(input_path)
    id_name = os.path.basename(input_path)
    dst_folder_reference = os.path.join(output_path, 'reference', id_name)
    dst_folder_raw = os.path.join(output_path, 'raw', id_name)

    # Make folder for train and raw images
    if not os.path.exists(dst_folder_reference):
        os.makedirs(dst_folder_reference)
    if not os.path.exists(dst_folder_raw):
        os.makedirs(dst_folder_raw)

    # If image name contains the string 'train' it will be used as the reference image of that ID
    train_index = 0
    raw_index = 0
    for file in files:
        if "train" in str.lower(file):
            src = os.path.join(input_path, file)
            dst = os.path.join(dst_folder_reference,
                               'train_{}.jpg'.format(str(train_index)))

            # Copy 'train' files to new destination
            shutil.copy(src, dst)
            train_index += 1
        else:
            src = os.path.join(input_path, file)
            dst = os.path.join(
                dst_folder_raw, 'raw_{}.jpg'.format(str(raw_index)))

            # Copy non 'train' files to new destination
            shutil.copy(src, dst)
            raw_index += 1
# Make csv file


def generate_csv(output_path, id_name):
    csv_modes = ['reference', 'raw']
    for csv_mode in csv_modes:
        csv_folder = os.path.join(output_path, 'csv_{}'.format(csv_mode))
        csv_output = os.path.join(csv_folder, (id_name + '.csv'))

        if not os.path.exists(csv_folder):
            os.makedirs(csv_folder)

        # arrange header for csv
        header = []
        for i in range(512):
            header.append('ftr '+str(i))  # will be header for features
        # will be path to image where the embedding is from
        header.append('img_ref')

        database_df = pd.DataFrame(columns=header)
        pd.DataFrame(database_df).to_csv(csv_output)
# Make classified directory per steps


def generate_filtered_dir(output_path, steps, id_name):
    filtered_path = os.path.join(output_path, 'filtered',  steps, id_name)

    # Make filtered directory
    if not os.path.exists(filtered_path):
        os.makedirs(filtered_path)

    # Make classified directory
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, 'below05')):
        os.makedirs(os.path.join(output_path, 'filtered',
                                 steps, id_name, 'below05'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '05to06')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '05to06'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '06to07')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '06to07'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '07to08')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '07to08'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '08to09')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '08to09'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '09to10')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '09to10'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, '10to11')):
        os.makedirs(os.path.join(
            output_path, 'filtered',  steps, id_name, '10to11'))
    if not os.path.exists(os.path.join(output_path, 'filtered',  steps, id_name, 'above11')):
        os.makedirs(os.path.join(output_path, 'filtered',
                                 steps, id_name, 'above11'))
# Embedding image


def image_embedding(output_path, id_name, category):
   # Get all images within the category folder
    def get_category_images():
        category_images = []
        images = os.listdir(folder_category)
        for image in images:
            img_path = os.path.join(folder_category, image)
            category_images.append(img_path)
        return category_images

    # Write embedding value to csv file
    def write_embedding_to_csv(embedding, csv_output):
        array_df = pd.DataFrame(columns=embedding)
        pd.DataFrame(array_df).to_csv(csv_output, mode='a', index=True)

    folder_category = os.path.join(output_path, category, id_name)

    # Write embedding value to csv file
    category_images = get_category_images()

    # Generate embeddings for each images, and write it into a csv
    csv_folder = os.path.join(output_path, 'csv_' + category)
    csv_output = os.path.join(csv_folder, (id_name + '.csv'))

    for category_image in category_images:
        img_name = os.path.basename(category_image)
        try:
            print('Try embedding file %s, %s from %s ' %
                  (img_name, category, id_name))
            embedding = core.generate_embedding(
                category_image)
            embedding = np.append(embedding, img_name)

            # Write embedding value to csv file
            write_embedding_to_csv(embedding, csv_output)
        except:
            print('failed to generate embedding file %s, %s from %s ' %
                  (img_name, category, id_name))
            pass
# Classifying image


def image_classifying(output_path, steps, id_name):
    # Read csv file
    def read_csv_db(csv_path):
        print('read csv', csv_path)
        with open(csv_path, newline='') as f:
            reader = csv.reader(f)
            embeddings = list(reader)

        reference_embeddings = []
        image_refs = []
        for index, embedding in enumerate(embeddings):
            if index == 0:
                pass
            else:
                image_refs.append(embedding[-1])
                embedding.pop(0)
                embedding.pop(-1)

                for i in range(len(embedding)):
                    embedding[i] = float(embedding[i])

                embedding = np.array(embedding)
                reference_embeddings.append(embedding)

        return reference_embeddings, image_refs

    # Locate the csv files
    csv_train_folder = os.path.join(output_path, 'csv_reference')
    csv_train_path = os.path.join(csv_train_folder, (id_name + '.csv'))

    csv_raw_folder = os.path.join(output_path, 'csv_raw')
    csv_raw_path = os.path.join(csv_raw_folder, (id_name + '.csv'))

    # Read the csv files
    reference_embeddings, reference_img = read_csv_db(csv_train_path)
    raw_embeddings, raw_img = read_csv_db(csv_raw_path)

    # Move reference to below 05
    index = 0
    references = os.listdir(os.path.join(output_path, 'reference', id_name))
    for reference in references:
        src = os.path.join(os.path.join(
            output_path, 'reference', id_name), reference)
        dst = os.path.join(os.path.join(output_path, 'filtered',
                                        steps, id_name, 'below05'), '{}.jpg'.format(str(index)))
        shutil.copy(src, dst)
        index += 1

    similar_imgs = []

    # Calculate distance
    for index_raw, raw_embedding in enumerate(raw_embeddings):
        print('processing', raw_img[index_raw], '===================')
        sim_list = []
        for index_ref, reference_embedding in enumerate(reference_embeddings):
            # Compute squared distance between embeddings
            dist = np.sum(np.square(reference_embedding - raw_embedding))
            # Compute cosine similarity between embedddings
            sim = np.dot(reference_embedding, raw_embedding.T)
            sim = 1 - sim

            if sim <= 0.5:
                print('SIMILAR IMAGE', raw_img[index_raw])
                # Print predictions
                print('image_ref =', reference_img[index_ref])
                print('image_raw =', raw_img[index_raw])
                # print('Distance = %f' %(dist))
                print('Similarity = %f' % (sim), '\n')
                sim_list.append(sim)
                # similar_imgs.append(raw_img[index_raw])
            else:
                print(raw_img[index_raw], 'not similar enough to',
                      reference_img[index_ref])
                print('Similarity = %f' % (sim), '\n')
                sim_list.append(sim)

        if len(sim_list) > 0:
            average_sim = sum(sim_list) / len(sim_list)
            print('average_sim for', raw_img[index_raw], 'is =', average_sim)

            # Classifying image
            src = os.path.join(os.path.join(
                output_path, 'raw', id_name), raw_img[index_raw])
            if average_sim < 0.5:
                similar_imgs.append(raw_img[index_raw])
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, 'below05'), '{}.jpg'.format(str(index)))
            elif average_sim >= 0.5 and average_sim < 0.6:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '05to06'), '{}.jpg'.format(str(index)))
            elif average_sim >= 0.6 and average_sim < 0.7:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '06to07'), '{}.jpg'.format(str(index)))
            elif average_sim >= 0.7 and average_sim < 0.8:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '07to08'), '{}.jpg'.format(str(index)))
            elif average_sim >= 0.8 and average_sim < 0.9:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '08to09'), '{}.jpg'.format(str(index)))
            elif average_sim >= 0.9 and average_sim < 1.0:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '09to10'), '{}.jpg'.format(str(index)))
            elif average_sim >= 1.0 and average_sim < 1.1:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, '10to11'), '{}.jpg'.format(str(index)))
            else:
                dst = os.path.join(os.path.join(output_path, 'filtered',
                                                steps, id_name, 'above11'), '{}.jpg'.format(str(index)))
            shutil.copy(src, dst)
            index += 1

    similar_imgs = list(set(similar_imgs))

    # Move similar image to reference folder
    folder_reference = os.path.join(output_path, 'reference', id_name)
    folder_raw = os.path.join(output_path, 'raw', id_name)

    folder_dst = os.path.join(output_path, 'reference', id_name)

    train_index = len(os.listdir(folder_reference))

    for similar_img in similar_imgs:
        src = os.path.join(folder_raw, similar_img)
        dst = os.path.join(folder_dst, 'train_{}.jpg'.format(str(train_index)))

        shutil.move(src, dst)
        train_index += 1
# End recursive if satisfying


def end_recursive(output_path, id_name, i, steps):
    total_photo_below05 = len(os.listdir(os.path.join(
        output_path, 'filtered', i, id_name, 'below05')))

    satisfy = total_photo_below05 > 40
    # satisfy = total_photo_below05 > 0.3 * len(os.listdir(s.path.join(output_path, 'raw', id_name)))
    if satisfy:
        return True
    else:
        return False


if __name__ == "__main__":
    main()
